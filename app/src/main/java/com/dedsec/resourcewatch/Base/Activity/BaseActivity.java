/*
 *	BaseActivity.java
 *	Author: Lucas Cota
 *	Description: Activity Defaults Base.
 *	Date: 2018-06-09
 *  Modified: 2018-06-09
 */

package com.dedsec.resourcewatch.Base.Activity;

import android.os.Bundle;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity
{
	// Resources
	public static Context activityContext;




	// Methods
	public void setActivityView(Context context, int layoutResID)
	{
		setActivityContext(context);
		setContentView(layoutResID);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		setPostCreateEvents();

		super.onPostCreate(savedInstanceState);
	}




	// Getters and Setters
	public static Context getActivityContext()
	{
		return activityContext;
	}

	public static void setActivityContext(Context activityContext)
	{
		if (BaseActivity.activityContext != activityContext)
		{
			BaseActivity.activityContext = activityContext;
		}
	}




	// Abstract
	public abstract void setPostCreateEvents();
}
