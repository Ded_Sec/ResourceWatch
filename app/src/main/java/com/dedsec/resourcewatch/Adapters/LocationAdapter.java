package com.dedsec.resourcewatch.Adapters;

import java.util.List;

import android.Manifest;
import android.os.Build;
import android.view.View;
import android.os.Looper;
import android.content.Intent;
import android.location.Location;
import android.provider.Settings;
import android.content.DialogInterface;
import android.annotation.SuppressLint;
import android.location.LocationManager;
import android.content.pm.PackageManager;
import android.support.v7.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.location.FusedLocationProviderClient;

import com.dedsec.resourcewatch.R;
import com.dedsec.resourcewatch.Workers.Localeman;
import com.dedsec.resourcewatch.Helpers.MessageHelper;
import com.dedsec.resourcewatch.Helpers.PermissionHelper;
import com.dedsec.resourcewatch.Base.Activity.BaseActivity;




@SuppressLint("MissingPermission")
public class LocationAdapter extends BaseActivity
		implements
		OnMapReadyCallback,
		GoogleMap.InfoWindowAdapter,
		GoogleMap.OnMapLongClickListener
{

	// Google Resources
	private GoogleMap googleMap;
	private Boolean isZommed = false;


	// Location Resources
	private Marker currentLocationMarker;
	private LocationRequest locationRequest;
	private FusedLocationProviderClient fusedLocationProviderClient;




	public void setLocationAdapter()
	{
		if (!((LocationManager) getSystemService(LOCATION_SERVICE)).isProviderEnabled(LocationManager.NETWORK_PROVIDER) || !((LocationManager) getSystemService(LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER))
		{
			MessageHelper.showToast(Localeman.Maps_ProviderUnavailableError);

			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
		else
		{
			fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
		}

		if (googleMap == null)
		{
			((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		if (fusedLocationProviderClient != null)
		{
			fusedLocationProviderClient.removeLocationUpdates(locationCallback);
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap)
	{
		this.googleMap = googleMap;
		this.googleMap.setInfoWindowAdapter(this);
		this.googleMap.setOnMapLongClickListener(this);

		locationRequest = new LocationRequest();
		locationRequest.setInterval(10000);
		locationRequest.setFastestInterval(10000);
		locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{
			if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
			{
				fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());

				googleMap.setMyLocationEnabled(true);
			}
			else
			{
				checkLocationPermission();
			}
		}
		else
		{
			fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());

			googleMap.setMyLocationEnabled(true);
		}
	}

	LocationCallback locationCallback = new LocationCallback()
	{
		@Override
		public void onLocationResult(LocationResult locationResult)
		{
			List<Location> locationList = locationResult.getLocations();

			if (locationList.size() > 0)
			{
				Location location = locationList.get(locationList.size() - 1);

				if (currentLocationMarker != null)
				{
					currentLocationMarker.remove();
				}


				LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

				MarkerOptions markerOptions = new MarkerOptions();
				markerOptions.position(latLng);
				markerOptions.title("Você está aqui!");
				markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

				currentLocationMarker = googleMap.addMarker(markerOptions);

				if (!isZommed)
				{
					googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

					isZommed = true;
				}
			}
		}
	};


	@Override
	public void onMapLongClick(LatLng latLng)
	{
		googleMap.addMarker(new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)));
	}




	@Override
	public View getInfoWindow(Marker marker)
	{
		return null;
	}




	@Override
	public void setPostCreateEvents()
	{

	}

	@Override
	public View getInfoContents(Marker marker)
	{
		return null;
	}




	private void checkLocationPermission()
	{
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
		{
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
			{
				new AlertDialog.Builder(this)
						.setTitle("Location Permission Needed")
						.setMessage("This app needs the Location permission, please accept to use location functionality")
						.setPositiveButton("OK", new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialogInterface, int i)
							{
								ActivityCompat.requestPermissions(LocationAdapter.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, PermissionHelper.PERMISSIONS_REQUEST_LOCATION);
							}
						})
						.create()
						.show();


			}
			else
			{
				ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, PermissionHelper.PERMISSIONS_REQUEST_LOCATION);
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
	{
		switch (requestCode)
		{
			case PermissionHelper.PERMISSIONS_REQUEST_LOCATION:
			{
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
				{
					if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
					{
						fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());

						googleMap.setMyLocationEnabled(true);
					}

				}
				else
				{
					MessageHelper.showToast("Permissão negada!");
				}
			}
		}
	}
}
