package com.dedsec.resourcewatch.Helpers;

import android.os.Build;
import android.text.Html;
import android.widget.Toast;

import com.dedsec.resourcewatch.Base.Activity.BaseActivity;

public class MessageHelper
{
	public static void showToast(String message)
	{
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
		{
			Toast.makeText(BaseActivity.getActivityContext(), Html.fromHtml("<b>" + message + "</b>"), Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(BaseActivity.getActivityContext(), Html.fromHtml("<b>" + message + "</b>", Html.FROM_HTML_MODE_LEGACY), Toast.LENGTH_LONG).show();
		}
	}

	// Toast Color
	public static void showToast(String message, String color)
	{
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
		{
			Toast.makeText(BaseActivity.getActivityContext(), Html.fromHtml("<font color='" + color + "'><b>" + message + "</b></font>"), Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(BaseActivity.getActivityContext(), Html.fromHtml("<font color='" + color + "'><b>" + message + "</b></font>", Html.FROM_HTML_MODE_LEGACY), Toast.LENGTH_LONG).show();
		}
	}
}
