package com.dedsec.resourcewatch.Views.Activity;

import android.os.Bundle;

import com.dedsec.resourcewatch.R;
import com.dedsec.resourcewatch.Adapters.LocationAdapter;

public class MapsActivity extends LocationAdapter
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_maps);

		setLocationAdapter();
	}
}
