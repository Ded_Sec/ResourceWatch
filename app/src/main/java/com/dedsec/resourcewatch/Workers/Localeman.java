package com.dedsec.resourcewatch.Workers;

import android.content.Context;

import com.dedsec.resourcewatch.Base.Activity.BaseActivity;
import com.dedsec.resourcewatch.R;

public class Localeman
{
	private static Context that = BaseActivity.getActivityContext();

	public static final String Maps_AddressUnavailable = that.getString(R.string.Maps_AddressUnavailable);
	public static final String Maps_ServicesError = that.getString(R.string.Maps_ServicesError);
	public static final String Maps_MarkerInfoError = that.getString(R.string.Maps_MarkerInfoError);
	public static final String Maps_ProviderUnavailableError = that.getString(R.string.Maps_AddressUnavailable);
}
